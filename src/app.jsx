import { MuiThemeProvider } from '@material-ui/core/styles';
import {
  CacheProvider,
  ThemeProvider as EmotionThemeProvider,
} from '@emotion/react';
import CssBaseLine from '@material-ui/core/CssBaseline';
import React from 'react';
import styled from '@emotion/styled';
import { theme } from '~/helpers/theme';
import cache from '~/server/cache';
import { fromPalette } from '~/helpers/styled-components-util';

const Root = styled.div`
  display: grid;
  height: 100vh;
  background: ${fromPalette('background.main')};
`;

const App = ({ children }) => {
  return (
    <CacheProvider value={cache}>
      <MuiThemeProvider theme={theme}>
        <EmotionThemeProvider theme={theme}>
          <CssBaseLine />
          <Root>{children}</Root>
        </EmotionThemeProvider>
      </MuiThemeProvider>
    </CacheProvider>
  );
};

export default App;
