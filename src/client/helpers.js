import { matchPattern } from 'url-matcher';

export function extractPathVariables(route) {
  const matchedObj = matchPattern(route.path, window.location.pathname);
  if (!matchedObj) return { status: false };

  const { paramNames = [] } = matchedObj;
  const { paramValues = [] } = matchedObj;

  return paramNames.reduce(
    (acc, curr, idx) => ({
      ...acc,
      [curr]: paramValues[idx],
    }),
    { status: true }
  );
}
