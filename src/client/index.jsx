import React, { useEffect } from 'react';
import { BrowserRouter } from 'react-router-dom';
import ReactDOM from 'react-dom';
import TilHomePage from '~/pages/today-i-learned';
import { TraverseRoutes } from '~/helpers/components/traverse-routes';
import { getQueryParams } from '~/helpers/random-functions/get-query-params';
import App from '~/app';
import ReadingPage from '~/pages/today-i-learned/reading-page';
import { extractPathVariables } from '~/client/helpers';

(async () => {
  try {
    const routesList = [
      {
        path: '/',
        exact: true,
        component: TilHomePage,
        data: {},
      },
      {
        path: '/page/:page_id',
        component: ReadingPage,
        data: {},
      },
    ];

    const promises = routesList.map(async (route) => {
      const pathVars = extractPathVariables(route);
      if (!pathVars.status) return route;

      const params = { ...pathVars, ...getQueryParams() };
      const { data: pageData } = await route.component.getProps(params);

      const Comp = route.component;
      return {
        ...route,
        data: pageData,
        component: (props) => <Comp {...props} data={pageData} />,
      };
    });

    const routes = await Promise.all(promises);

    const ClientApp = () => {
      useEffect(() => {
        const jssStyles = document.querySelector('#jss-server-side');
        if (jssStyles) {
          jssStyles.parentElement.removeChild(jssStyles);
        }
      }, []);

      return (
        <App>
          <BrowserRouter>
            <TraverseRoutes routes={routes} />
          </BrowserRouter>
        </App>
      );
    };

    ReactDOM.hydrate(<ClientApp />, document.getElementById('root'));
  } catch (err) {}
})();
