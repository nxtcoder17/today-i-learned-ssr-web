import styled from '@emotion/styled';
import { doSpacing } from '~/helpers/styled-components-util';

const TextOverImage = styled(({ children, ...props }) => (
  <div {...props}>
    <div>{children}</div>
  </div>
))`
  width: 400px;
  height: 300px;
  background: red;

  > div {
    padding: ${doSpacing(1)};
    background-image: url('https://www.fcbarcelona.com/photo-resources/2020/02/15/c9bf7f93-0b10-44cd-85d3-c4e31a6b24ff/mini_2020-02-15_FCBvsGETAFE_15.jpg?width=1200&height=750');
    background-size: cover;
    position: relative;
    height: 100%;

    :after {
      content: '';
      background: rgba(0, 0, 0, 0.45);
      background-size: cover;
      position: absolute;
      top: 0;
      bottom: 0;
      left: 0;
      right: 0;
    }
  }
`;
