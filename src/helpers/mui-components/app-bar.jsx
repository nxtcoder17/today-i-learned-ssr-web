import {
  AppBar,
  IconButton,
  InputBase,
  Toolbar,
  Typography,
} from '@material-ui/core';
import styled from '@emotion/styled';
import { Bell, Layers, Search } from 'react-feather';
import { useState } from 'react';
import { CssGrid } from '~/helpers/components/layouts/css-grid';

const AppBarWrapper = styled((props) => <AppBar position="fixed" {...props} />)`
  background: unset;
  box-shadow: unset;

  .toolbar {
    display: grid;
    grid-auto-flow: column;
    justify-content: space-between;
    color: inherit;
  }
`;

const LogoText = styled((props) => <Typography variant="h4" {...props} />)`
  font-weight: 800;
  font-family: 'Dancing Script', cursive;
`;

export const AppBarComp = () => {
  const [showSearchBar, setShowSearchBar] = useState(false);

  return (
    <>
      <AppBarWrapper>
        <Toolbar className="toolbar">
          <CssGrid autoFlow="column" gap={4} align="center">
            <LogoText>nxtcoder17</LogoText>

            <IconButton
              color="inherit"
              onClick={() => setShowSearchBar(!showSearchBar)}
            >
              <Search color="inherit" />
            </IconButton>

            {showSearchBar && (
              <InputBase
                sx={{ ml: 1, flex: 1 }}
                placeholder="Search GiiKi"
                inputProps={{ 'aria-label': 'search google maps' }}
              />
            )}
          </CssGrid>

          <CssGrid
            cols="repeat(2, min-content) auto"
            autoFlow="column"
            align="center"
            gap={2}
          >
            <IconButton size="medium">
              <Layers />
            </IconButton>

            <IconButton size="medium">
              <Bell />
            </IconButton>
          </CssGrid>
        </Toolbar>
      </AppBarWrapper>
      <Toolbar />
    </>
  );
};
