import axios from 'axios';

const { SERVER_URL } = process.env;
export const authorizedAxios = axios.create({
  baseURL: 'http://localhost:3999/til',
  withCredentials: true,
  headers: {
    post: {
      'Content-Type': 'application/json',
    },
  },
});
