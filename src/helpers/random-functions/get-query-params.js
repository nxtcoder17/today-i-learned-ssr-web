export function getQueryParams() {
  const qs = new URLSearchParams(window.location.search);
  const items = {};
  for (const [key, value] of qs.entries()) {
    items[key] = value;
  }

  return items;
}
