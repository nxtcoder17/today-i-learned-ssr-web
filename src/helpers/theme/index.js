import { createMuiTheme, responsiveFontSizes } from '@material-ui/core/styles';
import themePalette from './palette';
import typographyConfig from './typography';

const themeObj = createMuiTheme({
  ...themePalette,
  ...typographyConfig,
});

export const theme = responsiveFontSizes(themeObj, {
  factor: 2,
});
