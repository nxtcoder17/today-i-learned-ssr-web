import ReactMarkdown from 'react-markdown';
import RemarkGFM from 'remark-gfm';
import RemarkBreaks from 'remark-breaks';
import styled from '@emotion/styled';
import { doSpacing, fromPalette } from '~/helpers/styled-components-util';
import { MediaQuery } from '~/helpers/media-query';

const BlogPostWrapper = styled.div`
  font-size: 0.8rem;
  font-weight: 500;
  color: #edecda;

  // pre {
  // font-size: 0.7rem;
  // font-family: 'Fira Code', sans-serif;
  // font-feature-settings: ligatures;
  // font-variant-ligatures: normal;
  // overflow: inherit;
  // // code {
  // // white-space: pre-wrap;
  // // }
  // }

  // ${MediaQuery.md} {
  // overflow-x: unset;
  // code {
  // white-space: unset;
  // }
  // }

  // code[class*='language-'] {
  // padding: 0.5rem;
  // }

  // :not(pre) > code[class*='language-'],
  // pre[class*='language-'] {
  // border-radius: 15px;
  // }

  a {
    color: dodgerblue;
    text-decoration: none;
  }

  pre {
    code {
      font-family: 'Fira Code', Consolas, Monaco, 'Andale Mono', monospace !important;
    }
  }

  pre[class*='language-'] {
    display: grid;
    code {
      max-width: 100vw;
      overflow: auto !important;
      padding-bottom: ${doSpacing(0.5)};
    }
  }
`;

const BlogPost = ({ content }) => {
  return (
    <BlogPostWrapper>
      <ReactMarkdown plugins={[RemarkGFM, RemarkBreaks]}>
        {content || '## Still Loading the shit'}
      </ReactMarkdown>
    </BlogPostWrapper>
  );
};

BlogPost.Title = styled.h2`
  font-weight: 650;
  color: ${fromPalette('primary.main')};
`;

export default BlogPost;
