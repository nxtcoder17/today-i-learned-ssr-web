import styled from '@emotion/styled';
import { Typography } from '@material-ui/core';
import { doSpacing } from '~/helpers/styled-components-util';

const Wrapper = styled.div`
  padding: ${doSpacing(1)} ${doSpacing(4)};
  .logo {
    font-weight: 700;
    font-family: 'Dancing Script', cursive;
  }
`;

const Header = () => {
  return (
    <Wrapper>
      <Typography variant="h4" className="logo" color="primary">
        nxtcoder17
      </Typography>
    </Wrapper>
  );
};

export default Header;
