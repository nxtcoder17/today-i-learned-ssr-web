import styled from '@emotion/styled';
import { doSpacing, fromPalette } from '~/helpers/styled-components-util';

const Page = styled.div`
  display: grid;
  grid-template-rows: min-content 1fr;
  gap: ${doSpacing(2)};
  height: 100%;
  margin-bottom: ${doSpacing(3)};
  overflow-x: hidden;
  padding: 0 ${doSpacing(3)};

  *::-webkit-scrollbar {
    background: ${fromPalette('grey.main')} !important;
    height: ${doSpacing(0.75)} !important;
    border-radius: 15px;
  }

  *::-webkit-scrollbar-thumb {
    background: ${fromPalette('primary.main')} !important;
    border-radius: 15px;
  }
`;

export default Page;
