import styled from '@emotion/styled';
import { css } from '@emotion/react';
import { doSpacing, fromPalette } from '~/helpers/styled-components-util';

const Wrapper = styled.div`
  overflow: hidden;
  word-wrap: break-word;
  background: ${fromPalette('background.paper')};

  ${(props) =>
    props.image &&
    css`
      background-image: url(${props.image});
      background-size: cover;
      background-repeat: no-repeat;
      background-position: 50% 50%;
    `}

  display: grid;
  align-items: end;

  :after {
    content: '';
    position: absolute;
    height: 200px;
    width: 300px;
    background: rgba(0, 0, 0, 0.2);
  }

  .title {
    min-height: ${doSpacing(6)};
    position: relative;
    background: rgba(0, 0, 0, 0.35);
    backdrop-filter: blur(2px);

    top: 0;
    bottom: 0;
    left: 0;
    right: 0;

    display: grid;
    align-items: center;

    h4 {
      margin: 0;
      padding-left: ${doSpacing(2)};
    }
  }

  cursor: pointer;
  border-radius: 10px;

  color: ${fromPalette('primary.main')};
`;

const PostCard = ({ image, title, onClick, ...props }) => {
  return (
    <Wrapper onClick={onClick} image={image} {...props}>
      <div className="title">
        <h4>{title}</h4>
      </div>
    </Wrapper>
  );
};

export default PostCard;
