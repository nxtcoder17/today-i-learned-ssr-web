import TilHomePage from '~/pages/today-i-learned';
import ReadingPage from '~/pages/today-i-learned/reading-page';

export default {
  '/': TilHomePage,
  '/page': ReadingPage,
};
