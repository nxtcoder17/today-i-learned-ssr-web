import styled from '@emotion/styled';
import React from 'react';

import { useHistory } from 'react-router-dom';
import { doSpacing } from '~/helpers/styled-components-util';
import Header from '~/pages/components/header';
import Page from '~/pages/components/page';
import PostCard from '~/pages/components/post-card';
import { MediaQuery } from '~/helpers/media-query';

const POSTCARD_HEIGHT = '250px';
const POSTCARD_WIDTH = '300px';

const Container = styled.div`
  display: grid;
  gap: ${doSpacing(6)};
  grid-template-columns: repeat(auto-fill, min(100%, ${POSTCARD_WIDTH}));
  justify-content: center;

  ${MediaQuery.md} {
    justify-content: unset;
  }

  .postcard {
    height: ${POSTCARD_HEIGHT};
  }
`;

const HomePage = ({ data }) => {
  const history = useHistory();
  if (!data.results) return <></>;
  return (
    <Page>
      <Header />

      <Container>
        {data.results.map((item) => (
          <PostCard
            className="postcard"
            key={item.title}
            title={item.title}
            image={item.image}
            onClick={() => history.push(`/page/${item._id}`)}
          />
        ))}
      </Container>
    </Page>
  );
};

export default HomePage;
