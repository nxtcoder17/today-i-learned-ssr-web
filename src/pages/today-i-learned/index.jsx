import React from 'react';
import { authorizedAxios } from '~/helpers/random-functions/authorized-axios';
import HomePage from '~/pages/today-i-learned/home-page';

const getProps = () => authorizedAxios.get('/');

const TilHomePage = ({ data }) => {
  return <HomePage data={{ ...data, client: true }} />;
};

TilHomePage.Server = async (params) => {
  const { data } = await getProps(params);
  return <TilHomePage data={data} />;
};

TilHomePage.getProps = getProps;

export default TilHomePage;
