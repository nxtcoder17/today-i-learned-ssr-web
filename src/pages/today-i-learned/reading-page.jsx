import { useEffect, useState } from 'react';
import styled from '@emotion/styled';
import { useParams } from 'react-router-dom';
import { authorizedAxios } from '~/helpers/random-functions/authorized-axios';
import Page from '~/pages/components/page';
import Header from '~/pages/components/header';
import BlogPost from '~/pages/components/blog-post-helpers';

const Container = styled(({ children, ...props }) => (
  <div {...props}>
    <div className="main">{children}</div>
  </div>
))`
  display: grid;
  .main {
    display: grid;
  }
`;

const PageDetails = ({ blog }) => {
  return (
    <Page>
      <Header />
      <Container>
        <BlogPost.Title>{blog.title}</BlogPost.Title>
        <BlogPost content={blog.content} />
      </Container>
    </Page>
  );
};

const getProps = async ({ page_id: pageId }) =>
  authorizedAxios.get(`/id/${pageId}`);

const useServerProps = ({ props, getProps: getPropsFn }) => {
  const [data, setData] = useState(props);
  const { page_id: pageId } = useParams();
  useEffect(() => {
    if (props == null && pageId) {
      (async () => {
        const { data: result } = await getPropsFn({ page_id: pageId });
        setData(result);
      })();
    }
  }, [props, pageId]);

  return data;
};

const ReadingPage = ({ data }) => {
  const blog = useServerProps({ props: data, getProps });

  useEffect(() => {
    if (window.Prism && blog) {
      window.Prism.highlightAll();
    }
  }, [blog, window.Prism]);

  useEffect(() => {
    if (blog && window.Prism) {
      const inlineCodeBlocks = document.querySelectorAll(':not(pre)>code');
      inlineCodeBlocks.forEach((block) => block.classList.add('language-none'));
      window.Prism.highlightAll();
    }
  }, [blog, window.Prism]);

  if (!blog || !window.Prism) return <span>Fetching the Content</span>;

  return <PageDetails blog={blog} />;
};

ReadingPage.Server = async (params) => {
  const { data } = await getProps(params);
  return <PageDetails blog={data} />;
};

ReadingPage.getProps = getProps;

export default ReadingPage;
