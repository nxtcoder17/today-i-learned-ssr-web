import ReactDOMServer from 'react-dom/server';
import { ServerStyleSheets } from '@material-ui/core/styles';
import createEmotionServer from '@emotion/server/create-instance';
import cache from '~/server/cache';
import App from '~/app';

const wrapIntoHtml = async ({ body, title, styles }) => {
  return `
    <!doctype html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <!-- Open Sans Font -->
        <link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,600;0,700;0,800;1,300;1,400;1,600;1,700;1,800&display=swap" rel="stylesheet">
        
        <!-- Dancing Script -->
        <link href="https://fonts.googleapis.com/css2?family=Dancing+Script:wght@400;500;600;700&display=swap" rel="stylesheet">

        <!-- ABeeZee Font -->
        <link href="https://fonts.googleapis.com/css2?family=ABeeZee:ital@0;1&display=swap" rel="stylesheet">

        <link href="https://fonts.googleapis.com/css2?family=Fira+Code:wght@300;400;500;600;700&display=swap" rel="stylesheet">
        
        <link href="/assets/prism-darcula.css" rel="stylesheet" />

        <!--  Prism Settings -->

        <style id="jss-server-side">${styles}</style>
        <title>${title}</title>

        <style>
            :root {
              // font-family: 'Open Sans', sans-serif;
              font-family: 'ABeeZee', sans-serif;
              box-sizing: border-box;
            }

            body {
              font-family: 'ABeeZee', sans-serif;
            }

            :after,:before {
              box-sizing: border-box;
            }

        </style>
    </head>
    
    <body>
        <div id="root">${body}</div>
        <script async src="/dist/client/bundle.js"></script>
        <script async src="/assets/prism.js"></script>
    </body>
    </html>
  `;
};

export const { extractCritical } = createEmotionServer(cache);

export const sendPage = async (pageComp) => {
  // Reference: material UI SSR Examples: https://github.com/mui-org/material-ui/blob/next/examples/ssr

  const sheets = new ServerStyleSheets();

  const html = ReactDOMServer.renderToString(
    sheets.collect(<App>{pageComp}</App>)
  );

  // Grab the CSS from the sheets.
  const css = sheets.toString();

  // Grab the CSS from emotion
  const styles = extractCritical(html);

  return wrapIntoHtml({
    // body: ReactDOMServer.renderToString(html),
    body: html,
    title: 'React SSR Material UI',
    styles: `${css} ${styles.css}`,
  });
};

export const buildServerComp = async ({ params = {}, component: Comp }) => {
  const { data } = await Comp.getProps(params);
  return <Comp data={data} />;
};
