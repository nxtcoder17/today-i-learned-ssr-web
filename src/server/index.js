import express, { Router } from 'express';
import path from 'path';
import { StatusCodes } from 'http-status-codes';
import createError from 'http-errors-lite';
import assert from 'assert';
import pages from '../pages';

import { sendPage } from '~/server/helpers';

const app = express();

app.use('/dist', express.static(path.resolve(__dirname, '../../public/dist')));
app.use(
  '/assets',
  express.static(path.resolve(__dirname, '../../public/assets'))
);

app.use((req, res, next) => {
  res.setHeader('content-type', 'text/html;charset=UTF-8');
  next();
});

const router = Router();

router.get('/favicon.ico', (req, res) => {
  res.send(StatusCodes.OK);
});

router.get('/page/:page_id', async (req, res) => {
  const { page_id: pageId } = req.params;
  const Page = pages['/page'];
  assert(
    Page != null,
    createError(StatusCodes.NOT_FOUND, 'Requested Page not found')
  );

  const ServerComp = await Page.Server({ page_id: pageId });
  return res.send(await sendPage(ServerComp));
});

router.get('/', async (req, res) => {
  const Page = await pages['/'];
  const ServerComp = await Page.Server();
  res.send(await sendPage(ServerComp));
});

app.use('/', router);

(async () => {
  await app.listen(3000);
  console.log('FrontEnd Server Started ...');
})();
