const webpack = require('webpack');
const path = require('path');
const DotEnv = require('dotenv-webpack');
const CompressionPlugin = require('compression-webpack-plugin');
const zlib = require('zlib');

module.exports = (env = {}) => {
  const { isProduction = false } = env;

  const productionOnly = {
    mode: 'production',
    optimization: {
      minimize: true,
    },
    plugins: [
      new DotEnv({
        path: isProduction ? './env/production.env' : './env/development.env',
      }),
      new webpack.HotModuleReplacementPlugin(),
      new CompressionPlugin({
        filename: '[path][base].gz',
        algorithm: 'gzip',
        test: /\.js$|\.css$|\.html$/,
        threshold: 10240,
        minRatio: 0.8,
      }),
      new CompressionPlugin({
        filename: '[path][base].br',
        algorithm: 'brotliCompress',
        test: /\.(js|css|html|svg)$/,
        compressionOptions: {
          params: {
            [zlib.constants.BROTLI_PARAM_QUALITY]: 11,
          },
        },
        threshold: 10240,
        minRatio: 0.8,
      }),
    ],
  };

  const developmentOnly = {
    mode: 'development',
    devtool: 'eval-source-map',
    plugins: [
      new DotEnv({
        path: isProduction ? './env/production.env' : './env/development.env',
      }),
      new webpack.HotModuleReplacementPlugin(),
    ],
  };

  return {
    ...(isProduction ? productionOnly : developmentOnly),
    entry: ['babel-polyfill', './src/client/index.jsx'],
    target: 'web',
    output: {
      path: path.resolve(__dirname, 'public/dist/client'),
      filename: 'bundle.js',
      chunkFilename: '[chunkhash].bundle.js',
      publicPath: '/dist/',
    },

    module: {
      rules: [
        {
          test: /.*\.jsx?/,
          exclude: /node_modules/,
          use: {
            loader: 'babel-loader',
            options: {
              rootMode: 'upward',
            },
          },
        },
        {
          test: /\.s?[ac]ss$/i,
          use: [
            // Creates `style` nodes from JS strings
            'style-loader',
            // Translates CSS into CommonJS
            'css-loader',
            // Compiles Sass to CSS
            'sass-loader',
          ],
        },
        {
          test: /\.(png|jpe?g|svg|gif)$/,
          use: [
            {
              loader: 'file-loader',
              options: {
                name: 'images/[hash]-[name].[ext]',
              },
            },
          ],
        },
      ],
    },
    resolve: {
      extensions: ['.jsx', '.js'],
      alias: {
        '~': path.resolve(__dirname, 'src'),
      },
      fallback: {
        path: false,
        fs: false,
        stream: false,
        http: false,
        crypto: false,
        zlib: false,
      },
      modules: ['node_modules'],
    },
    devServer: {
      port: env.PORT || 9999,
      host: '0.0.0.0',
      contentBase: path.resolve(__dirname, './public/'),
      disableHostCheck: true,
      historyApiFallback: true,
      hot: true,
    },
  };
};
